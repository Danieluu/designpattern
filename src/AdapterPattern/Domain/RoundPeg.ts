import { Peg } from "../Abstract/Peg";

export class RoundPeg implements Peg {
    private radius!: number;

    constructor(radius: number) {
        this.radius = radius;    
    };

    getRadius() {
        return this.radius;
    };
}