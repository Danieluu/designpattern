import { RoundPeg } from "./RoundPeg";
import { Peg } from "../Abstract/Peg";

export class RoundHole {
    private radius!: number;

    constructor(radius: number) {
        this.radius = radius;
    };

    getRadius() {
        return this.radius;
    }

    fits(peg: Peg) {
        return this.radius >= peg.getRadius();
    };
}