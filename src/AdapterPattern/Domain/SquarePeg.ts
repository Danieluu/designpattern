export class SquarePeg {
    private width!: number;

    constructor(width: number) {
        this.width = width;    
    };

    getWidth() {
        return this.width;
    };
};