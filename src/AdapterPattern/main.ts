import { RoundHole } from "./Domain/RoundHole";
import { RoundPeg } from "./Domain/RoundPeg";
import { SquarePeg } from "./Domain/SquarePeg";
import { SquarePegAdapter } from "./Adapter/SquarePegAdapter";

const hole = new RoundHole(5);
const rPeg = new RoundPeg(5);
console.log(hole.fits(rPeg));

const smallSqPeg = new SquarePeg(5);
const largeSqPeg = new SquarePeg(10);

const smallSqPegAdapter = new SquarePegAdapter(smallSqPeg);
const largeSqPegAdapter = new SquarePegAdapter(largeSqPeg);
console.log(hole.fits(smallSqPegAdapter));
console.log(hole.fits(largeSqPegAdapter));