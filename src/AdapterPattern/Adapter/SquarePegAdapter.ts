import { SquarePeg } from "../Domain/SquarePeg";
import { Peg } from "../Abstract/Peg";

export class SquarePegAdapter implements Peg {
    private peg!: SquarePeg;

    constructor(peg: SquarePeg) {
        this.peg = peg;
    };

    getRadius() {
        return this.peg.getWidth() * Math.sqrt(2) / 2;
    };
}