export abstract class Peg {
    abstract getRadius(): number;
}