import { TV } from "./Domain/TV";
import { RemoteControl } from "./Domain/RemoteControl";

let tv = new TV();
let remote = new RemoteControl(tv);
remote.togglePower();
console.log(JSON.stringify(tv));