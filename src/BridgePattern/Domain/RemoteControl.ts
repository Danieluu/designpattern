import { Device } from "../Interface/Devices";

export class RemoteControl {
    protected device!: Device;

    constructor(device: Device) {
        this.device = device;
    };

    togglePower() {
        if (this.device.isEnable()) this.device.disable();
        this.device.enable();
    };

    volumeUp() {
        this.device.setVolume(this.device.getVolume() + 1);
    };

    volumeDown() {
        this.device.setVolume(this.device.getVolume() - 1);
    };

    channelUp() {
        this.device.setChannel(this.device.getChannel() + 1);
    };

    channelDown() {
        this.device.setChannel(this.device.getChannel() - 1);
    };
}