import { Device } from "../Interface/Devices";

export class TV implements Device {
    private _enable: boolean = false;
    private _channel: number = 1;
    private _volume: number = 5;

    isEnable(): boolean {
        return this._enable === true;
    };

    enable(): void {
        this._enable = true;
    };

    disable(): void {
        this._enable = false;
    };
    
    getVolume(): number {
        return this._volume;
    };

    setVolume(vol: number): void {
        this._volume = vol;
    };

    getChannel(): number {
        return this._channel;
    };

    setChannel(channel: number): void {
        this._channel = channel;
    };

}