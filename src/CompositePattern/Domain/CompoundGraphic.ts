import { Graphic } from "../Interface/Graphic";

export class CompoundGraphic implements Graphic {
    children: Graphic[] = [];

    add(child: Graphic) {
        this.children.push(child);
    };

    remove(child: Graphic) {
        // remove child
    }

    move(x: number, y: number): void {
        this.children.forEach(item => item.move(x,y));
    };

    draw(): void {
        this.children.forEach(item => item.draw());
    };

}