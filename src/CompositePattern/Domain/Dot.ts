import { Graphic } from "../Interface/Graphic";

export class Dot implements Graphic {
    x: number = 0;
    y: number = 0;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    move(x: number, y: number): void {
        this.x += x;
        this.y += y;
    };

    draw(): void {
        console.log(`The Dot x: ${this.x}, y: ${this.y}`);
    };

}