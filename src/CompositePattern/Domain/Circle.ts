import { Dot } from "./Dot";

export class Circle extends Dot {
    private radius: number = 0;

    constructor(x: number, y: number, radius: number) {
        super(x, y);
        this.radius = radius;
    };

    draw() {
        console.log(`The Circle x: ${this.x}, y: ${this.y}, radius: ${this.radius}`);
    };
}