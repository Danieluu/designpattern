import { Builder } from "../Interface/Builder";
import { Car } from "../Domain/Car";

export class CarBuilder implements Builder {
    private  car!: Car;

    constructor(){
        this.reset();
    }

    reset(): void {
        this.car = new Car();
    };

    setSeats(seats: number): void {
        this.car.seats = seats;
    };

    getProduct(): Car {
        let product = this.car;
        this.reset();
        return product;
    };
    
}