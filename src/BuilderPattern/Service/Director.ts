import { Builder } from "../Interface/Builder";

export class Director {
    private builder!: Builder

    setBuilder(builder: Builder){
        this.builder = builder;
    };

    constructSportsCar(builder: Builder){
        builder.reset();
        builder.setSeats(2);
    }
}