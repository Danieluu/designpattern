import { Director } from "./Service/Director";
import { CarBuilder } from "./Service/CarBuilder";

const director = new Director();

const builder = new CarBuilder();
director.constructSportsCar(builder);
const car = builder.getProduct();
console.log(JSON.stringify(car));