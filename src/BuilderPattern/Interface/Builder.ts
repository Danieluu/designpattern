export abstract class Builder {
    abstract reset(): void;
    abstract setSeats(seats: number): void;
    abstract getProduct(): void;
};