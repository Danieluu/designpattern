import { Shape } from "../Interface/shape";

export class Circle extends Shape {
    radius!: number;

    constructor(source?: Circle) {
        if (!source) return;

        super(source);
        this.radius = source.radius;
    };

    clone() {
        return new Circle(this);
    };
};

