import { Shape } from "../Interface/shape";

export class Rectangle extends Shape {
    width!: number;
    height!: number;

    constructor(source?: Rectangle) {
        if(!source) return;
        
        super(source);
        this.width = source.width;
        this.height = source.height;
    };

    clone() {
        return new Rectangle(this);
    };
};