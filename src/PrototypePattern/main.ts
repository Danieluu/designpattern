import { Shape } from "./Interface/shape";
import { Circle } from "./Domain/Circle";
import { Rectangle } from "./Domain/Rectangle";

const shapes: Shape[] = [];

const circle = new Circle();
circle.x = 10;
circle.y = 10;
circle.radius = 20;

const anotherCircle = new Circle(circle);
shapes.push(anotherCircle);

const rectangle = new Rectangle();
rectangle.x = 5;
rectangle.y = 5;
rectangle.width = 10;
rectangle.height = 100;

const anotherRectangle = new Rectangle(rectangle);
shapes.push(anotherRectangle);

shapes.forEach(item => {
    console.log(JSON.stringify(item));
});