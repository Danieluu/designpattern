export class Database {
    private static instance: string;

    constructor() {
        console.log('connection database...');
    };

    static getInstance() {
        if (!Database.instance) {
            console.log("not connect database");
            Database.instance = 'had connection';
        }
        return new Database();
    }

    query() {
        console.log("query database");
    };
};